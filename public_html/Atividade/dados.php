<?php
    function formatNome($nome) {
        if($nome != "")
            return ucwords(strtolower($nome));
        else
            return "Vazio";
    }

    function formatEmail($email) {
        if($email != "")
            return strtolower($email);
        else
            return "Vazio";
    }

    function formatEndereco($endereco) {
        if($endereco != "")
            return ucwords(strtolower($endereco));
        else
            return "Vazio";
    }
    function formatProfissao($profissao) {
        if($profissao != "Vazio")
            return ucwords(strtolower($profissao));
        else
            return "Vazio";
    }
    function formatGenero($genero) {
        if($genero != "")
            return $genero;
        else
            return "Vazio";
    }

    function formatSenha($senha) {
        if($senha != "")
            return $senha;
        else
            return "Vazio";
    }
    function formatInteresses($interesses) {
        $aux = "";
        if($interesses != ""){
            foreach ($interesses as &$int){
                $aux = $aux.$int." || ";
            }
            return $aux;   
        }else
            return "Vazio";
    }

    function formatFaleConosco($fale) {
        if($fale != "")
            return $fale;
        else
            return "Vazio";
    }

    function validarSenha($senha1) {
        if($senha1 == "Vazio"){
            return "";
        }
        if (strlen($senha1) < 6 ) {
            return "É recomendado uma senha com mais de 6 caracteres!";
        } else {
            return "";
        }
    }

    function validarConfirmacaoSenha($senha1, $senha2) {
        if($senha1 == "Vazio"){
            return "Impossivel comparar! Senha VAZIA!";
        }
            
        if ($senha1 === $senha2){
            return "Igual";
        
        }else {
            return "Diferente";
        }
    }

    $txtNome = formatNome($_POST["nome"]);
    $txtEmail = formatEmail($_POST["email"]);
    $txtEndereco =formatEndereco($_POST["endereco"]);
    $txtProfissao = formatProfissao($_POST["selProf"]);
    $interesses = formatInteresses($_POST["interesses"]);
    $fale = formatFaleConosco($_POST["faleConosco"]);
    $txtGenero = formatGenero($_POST["genero"]);
    $txtSenha1 = formatSenha($_POST["senha1"]);
    $txtSenha2 = formatSenha($_POST["senha2"]);

    $mensagemSenha = validarSenha($txtSenha1);
    $mensagemConfirmacaoSenha = validarConfirmacaoSenha($txtSenha1, $txtSenha2);
?>

<!DOCTYPE html>
<html lang="PT-BR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dados Coletados</title>
    <style>
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
            padding: 5px;
        }
    </style>
</head>
<body>
<h2>Dados do Formulário</h2>
<table>
    <tr>
        <th>Nome</th>
        <td>
            <?php echo $txtNome; ?>
        </td>
    </tr>
    <tr>
        <th>Email</th>
        <td><?php echo $txtEmail; ?></td>
    </tr>
    <tr>
        <th>Endereço</th>
        <td><?php echo $txtEndereco; ?></td>
    </tr>
    <tr>
        <th>Profissão:</th>
        <td><?php echo $txtProfissao; ?></td>
    </tr>
    <tr>
        <th>Gênero</th>
        <td><?php echo $txtGenero; ?></td>
    </tr>
    <tr>
        <th>Interesses:</th>
        <td><?php echo $interesses; ?></td>
    </tr>
    <tr>
        <th>Fale Conosco:</th>
        <td><?php echo $fale; ?></td>
    </tr>
    <tr>
        <th>Senha</th>
        <td><?php echo $txtSenha1; ?> <?php echo $mensagemSenha; ?></td>
    </tr>
    <tr>
        <th>Confirmação de Senha</th>
        <td><?php echo $mensagemConfirmacaoSenha; ?></td>
    </tr>
</table>

</body>
</html>
