<!DOCTYPE html>
<html lang="bt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Atividade Avaliativa</title>
    <link rel="stylesheet" href="style.css" />
</head>
<body>
   <div class="principal">
        
        <form method="POST" action="dados.php">
            <h1>FORMULÁRIO DE INSCRIÇÃO</h2>
            <div class="nome">
                <label for="nome">Nome:</label>
                <input type="text" name="nome" >
            </div>

            <div class="email">
                <label for="email">Email:</label>
                <input type="email" name="email" >
            </div>

            <div class="endereco">
                <label for="text">Endereço:</label>
                <input type="text" name="endereco" >
            </div>

            <div class="profissoes">
                <label for="profissao">Profissão:</label>
                <select name = "selProf">
                    <option VALUE="Vazio">SELECIONE UMA PROFISSÃO</option>
                    <option VALUE="Engenheiro de Software">Engenheiro de Software</option>
                    <option VALUE="Analista de Segurança de Informação">Analista de Segurança de Informação</option>
                    <option VALUE="Analista de Sistemas">Analista de Sistemas</option>
                    <option VALUE="Administrador de Sistemas">Administrador de Sistemas</option>
                    <option VALUE="Administrador do banco de dados (DBA)">Administrador do banco de dados (DBA)</option>
                    <option VALUE="Gestor de tecnologias da informação">Gestor de tecnologias da informação</option>
                    <option VALUE="Arquiteto de redes">Arquiteto de redes</option>
                    <option VALUE="Desenvolvedor">Desenvolvedor</option>
                </select>
            </div>

            <div class="gen">
                <label for="genero">Gênero:</label>
                <div class="class-radio">
                    <input TYPE="radio" NAME="genero" VALUE="masculino"  onclick="showInput()">Masculino</input>
                    <input TYPE="radio" NAME="genero" VALUE="feminino"  onclick="showInput()">Feminino</input>
                    <input TYPE="radio" NAME="genero" VALUE="Outro" onclick="showInput()">Outro (Qual?)</input>
                    <div id="outroInput" style="display: none;">
                        <input type="text" name="genero" placeholder="Digite seu gênero">
                    </div>
                    <script>
                        function showInput() {
                            var outroInput = document.getElementById("outroInput");
                            if (document.querySelector('input[name="genero"]:checked').value === "Outro") {
                                outroInput.style.display = "block";
                            } else {
                                outroInput.style.display = "none";
                            }
                        }
                    </script>
                </div>
            </div>

            <div class="interesse">
                <label for="interesses">Área de Interesse:</label>
                <div class="opcoes">
                    <input TYPE="checkbox" NAME="interesses[]" value="Banco de dados">Banco de dados</input>
                    <input TYPE="checkbox" NAME="interesses[]" value="Engenharia de software">Engenharia de software</input>
                    <input TYPE="checkbox" NAME="interesses[]" value="Redes de computadores">Redes de computadores</input>
                    <input TYPE="checkbox" NAME="interesses[]" value="Desenvolvimento de Jogos">Desenvolvimento de Jogos</input>
                    <input TYPE="checkbox" NAME="interesses[]" value="Programação back-end">Programação back-end</input> 
                    <input TYPE="checkbox" NAME="interesses[]" value="Programação front-end">Programação front-end</input> 
                    <input TYPE="checkbox" NAME="interesses[]" value="Mineração de dados">Mineração de dados</input>
                </div>
                
            </div>

            <div class="FaleConosco">
                <label for="fConosco">Fale conosco:</label>
                <textarea name="faleConosco" rows="10" cols="80" placeholder="Deixe o que quiser aqui!"></textarea>
            </div>
            
            <div class="inf">
                <input TYPE="checkbox" NAME="infos" >Deseja receber nossas promoções?</input>
            </div>  
            
            <div class="senhas">
                <label for="senha_1">Senha:</label><br>
                <input type="password" name="senha1" />

                <label for="senha_2">Confirme a senha:</label>
                <input type="password" name="senha2"  />
            </div>

            <div class="button">
                <button type="submit" value="Enviar">Confirmar</button>
            </div>
                
        </form>
   </div> 
</body>
</html>