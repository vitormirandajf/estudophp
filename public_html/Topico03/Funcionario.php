<?php

    require_once("Pessoa.php");
    require_once("Web.php");

    class Funcionario extends Pessoa implements Web {

        private int $matricula;

        public function __construct(string $nome, float $salario, int $matricula) {
            parent::__construct($nome, $salario);

            $this->matricula = $matricula;
        }

        public function __destruct() {
            echo "Destruindo o objeto {$this->nome}";
        }

        public function getSalario() : float {
            return $this->salario;
        }

        public function setSalario(float $salario) {
            if ($salario >= 1000) {
                $this->salario = $salario;
            }
        }

        public function getNome() : string {
            return $this->nome;
        }

        public function setNome(string $nome) {
            $this->nome = $nome;
        }
        function imprime(){
            echo "<p> {$this->nome} </p>";
            echo "<p> {$this->salario} </p>";
            echo "<p> {$this->matricula} </p>";
        }

        function __get($nome){
            if($nome == "identidade"){
                return $this->matricula;
            }
            
        }

        function __set($nome,$valor){
            if($nome == "identidade"){
                $this->matricula = $valor;
            }
        }

        function __call($nome,$parametros){
            if($nome == "setIdentidade"){
                $this->matricula = $parametros[0];
            }
        }

        

    }