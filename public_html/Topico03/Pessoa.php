<?php

    abstract class Pessoa {
        
        protected string $nome;
        protected float $salario;

        public function __construct(string $nome, float $salario) {
            $this->nome = $nome;
            $this->salario = $salario;
        }

        abstract public function setSalario(float $salario);
        abstract public function getSalario() : float;
        abstract public function setNome(string $nome);
        abstract public function getNome() : string;

        function imprimeNome(){
            echo "<p> {$this->nome} </p>";
        }
    }