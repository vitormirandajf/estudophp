<?php

    class UsuarioSessao {

        public $nome;
        public $login;

        public function __construct($nome, $login) {
            $this->nome = $nome;
            $this->login = $login;

            echo "<p>Novo objeto.</p>";
        }

        public function salvar(string $nome) {
            $this->nome = $nome;

            echo "<p> salvar </p>";
        }

        public function ler() : string {
            echo "<p> ler UsuarioSessao</p>";

            return $this->nome;
        }
        
    }