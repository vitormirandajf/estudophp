<?php

    spl_autoload_register(function ($classe) {
        require_once str_replace('\\','/',$classe.'.php');
    });

    use Seguranca\Conta as ContaS;
    use Financas\Conta as ContaF;

    $func1 = new Funcionario("Juquinha", 1500.5, 123);
    $func2 = new Funcionario("Maria", 1500.5, 123);

    $dep = new Departamento();

    $dep->addFuncionario($func1);
    $dep->addFuncionario($func2);

    $dep->addFuncionario($func1);
    var_dump($func1);

    echo $func1->nome;

    // $seguranca = new ContaF();

    // $financas = new ContaS();

    // $usu1 = new UsuarioSessao("Pedro", "pdr");

    // $usu1->salvar("Pedro A.");
    // $usu1->ler();

    // var_dump($usu1);

    // $usuPerm = new UsuarioSessaoPermissao("Augusto", "aug", 10);
