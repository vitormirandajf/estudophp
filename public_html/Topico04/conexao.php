<?php

class DataBase{

}

static $db = null;

public function __construct(){
    if(self::$db == null){
        self::$db = new mysqli('mariadb','root','root','tads');

        if( self::$db->connect_errno > 0){
            die("Ocorreu um erro {$db->connection_error}");
        }
    }
}

function getConnection(){
    return self::$db;
}

function Close(){
    return self::$db->close();
}