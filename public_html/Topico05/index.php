<?php

    require_once("Database.php");
    require_once("Aluno.php");

    $alunoAntigo = new Aluno(1, "Pedro", 777999, true);

    $db = new Database();
    
    $con = $db->getConnection();
    
    // $sql = "SELECT id, nome, matricula FROM tads.alunos;";
    
    $sql = "SELECT id, nome, matricula FROM tads.alunos WHERE id = 1;";

    $alunos = $con->query($sql);
    
    // $alunos->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, "Aluno");
    
    $alunos->setFetchMode(PDO::FETCH_INTO|$alunoAntigo);
    $alunos->execute();

    $alunoAntigo = $alunos->fetch();

    var_dump($alunoAntigo);

    // echo "Quantidade de alunos: {$alunos->rowCount()}";

    // foreach ($alunos as $aluno) {
    //     $aluno->imrpimeDados();
    // }