<?php
    if (isset ($_GET["alterar"])) {
       $id = $_GET["alterar"];

       require_once "RecadoDAO.php";

       $sql = "SELECT id, nome, email, cidade, texto FROM tads.recados WHERE id = :id;";
       $stmt = $con->prepare($sql);
       $stmt->bindValue(':id',$id);
       
       $stmt->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, "RecadoDAO");
       
       if($stmt->execute()){
          $recado = $stmt->fetch();
       }
    }
?>

<form action="<?= isset($recado->id)?"?atualizar=$recado->id":""?>" method="post"> <!--deixando o action vazio e a propria pagina processa o arquivo-->
    Nome: 
    <input type="text" name="nome" id="nome" value="<?= $recado->nome??'' ?>"><br> <!--esse ?? pergunta: existe a variavel anteriormente dita? se sim, preencha com ela, se não preencha com o que vou colocar depois do ??, no caso foi vazio ''-->
    E-mail: 
    <input type="text" name="email" id="email" value="<?= $recado->email??'' ?>"><br>
    Cidade: 
    <input type="text" name="cidade" id="cidade" value="<?= $recado->cidade??'' ?>"><br>
    <textarea name="recado" id="recado" cols="30" rows="10" > <?= $recado->texto??'' ?> </textarea><br> <!--textarea não tem value, nesse caso fica após a abertura-->

    <input type="submit" value="<?= isset($recado->id)?"Atualizar":"Cadastrar" ?>" name="enviar"/>
</form>

<?php

    if (isset($_POST["enviar"])):
        $nome = $_POST["nome"];
        $email = $_POST["email"];
        $cidade = $_POST["cidade"];
        $texto = $_POST["recado"];
        

        if ($_POST["enviar"] == "Cadastrar"):

            $sql = "INSERT INTO tads.recados
            (nome, email, cidade, texto)
            VALUES(:nome,:email, :cidade, :texto);";
            $stmt = $con->prepare($sql);
            
 
        elseif ($_POST["enviar"] == "Atualizar"):

            $id = $_GET["atualizar"];
            $sql = "UPDATE tads.recados SET nome=:nome, email=:email, cidade=:cidade, texto=:texto WHERE id=:id;";
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':id',$id);
        endif;

        $stmt->bindValue(':nome',$nome);
        $stmt->bindValue(':email',$email);
        $stmt->bindValue(':cidade',$cidade);
        $stmt->bindValue(':texto',$texto);
        $stmt->execute();

    endif;

?>
