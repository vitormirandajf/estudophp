<?php
    //arrays

    $precos = [];

    $valores = array("telefone" => 900, "biscoito" => 223);

    $precos[3] = 1.75;
    $precos[2] = 2.56;
    $precos[] = 8.43;
    $precos[32] = 1.32;
    $precos["biscoito"] = 900;

    var_dump($precos);

    var_dump($valores);

    //ordena pelo indice
    ksort($precos);
    
    var_dump($precos);
    
    echo "<br>O preço do biscoito é: R$".$precos["biscoito"];

    $stas = array("MD", "BH", "KK", "HM","JP");

    $qnt = count($stas);
    for($var = 0; $var < $qnt; $var++){
        echo "<br>Indice: ".$var." valor: ".$stas[$var];
    }
    
    //iteração para array com indices não sequenciais
    foreach ( $precos as $key => $value){
        echo "<br> $key : $value";
    }
    
    //sem a chave
    foreach ( $precos as $var){
        echo "valor: $var <br>";
    }



    
