<?php
    $nome = "Vitor";
    echo $nome;
    echo gettype($nome);

    $Nome = &$nome; //passando variavel por referencia.
    echo $Nome;

    echo "<br>";
    echo "Será mostrado o valor da variavel:<strong> $nome </strong> <br>";
    echo 'Será mostrado o valor da variavel: $nome';
    echo "<br>";
    for($var = 0; $var <= 20; $var++){
        echo $var."*".$var."= ".$var*$var."<br>";
    }
    $i = 0;
    echo "<br>";
    echo "While";
    echo "<br>";
    while ($i<=20) {
        echo $i."*".$i."= ".$i*$i."<br>";
        $i++;
    }
?>